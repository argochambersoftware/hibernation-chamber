# Hibernation Chamber
### Argochamber Interactive 2016

* * *

Hibernation chamber is a wrapper library for java's Hibernates.

Example of use (__Quickstart guide__):
```java

// This is the reference, can be where you want.
HibernationChamber hibernation;

public void mainMethod(){
	
	// This builds a new object with a brand new session!
	hibernation = new HibernationChamber();
	
	// Do the fine here...
	
	// Starts a new TRANSACTION
	hibernation.start();
	
	//Transaction things...
	
	// Ends the TRANSACTION
	hibernation.end();
	
	// Do want to end the session? No problem!
	hibernation.closeSession();
	
	// Want a new session? Also there's a fine way:
	hibernation = new HibernationChamber();
}

```