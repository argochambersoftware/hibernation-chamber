/*
 * Argochamber Interactive 2016
 */
package com.argochamber.hibernates.hibernationchamber;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * This is an hibernation chamber.
 * <p>
 * It will store sessions of the hibernate and transaction data.
 * </p>
 * The usage is like:
 * <p>
 * HibernateChamber ses = new HibernateChamber(); ses.start(); // DO Hibernate
 * things here. ses.end();
 * </p>
 *
 * @author Pablo
 */
public class HibernationChamber {

    private Session session;
    private Transaction tx;
    private SessionFactory factory;
    private Configuration configuration;
    private ServiceRegistry serviceRegistry;

    /**
     * <h1>Hibernation Chamber</h1>
     * <h3>Session builder</h3>
     * <p>
     * This will create automatically a session.
     * </p>
     */
    public HibernationChamber() {
        try {
            configuration = new Configuration();
            serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                    configuration.getProperties()).build();
            // We have to rely on deprecated methods, for now.
            factory = new Configuration().configure().buildSessionFactory(); //serviceRegistry
        } catch (HibernateException ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }
        this.session = factory.openSession();
    }

    /**
     * <h1>Transaction Start</h1>
     * <p>
     * This <b>starts</b> a <i>new transaction</i>, so in order to make it work you must
     * call those statements in <u>order</u> like:
     * </p>
     * <ul>
     * <li><strong>hibernation.start();</strong></li>
     * <li>//Do whatever you like here</li>
     * <li>(...)</li>
     * <li>hibernation.end();</li>
     * </ul>
     * @see #end()
     */
    public void start() {
        this.tx = this.session.beginTransaction();
    }

    /**
     * Getter for the session.
     * @return 
     */
    public Session getSession() {
        return this.session;
    }

    /**
     * Getter for the current transaction.
     * @return 
     */
    public Transaction getTx() {
        return this.tx;
    }
    
    /**
     * <h1>Transaction End</h1>
     * <p>
     * This <b>ends</b> a <i>started transaction</i>, so in order to make it work you must
     * call those statements in <u>order</u> like:
     * </p>
     * <ul>
     * <li>hibernation.start();</li>
     * <li>//Do whatever you like here</li>
     * <li>(...)</li>
     * <li><strong>hibernation.end();</strong></li>
     * </ul>
     * @see #start()
     */
    public void end() {
        try {
            this.tx.commit();
        } catch (Exception e) {
            System.err.println("TRANSACTION FAILURE!! Reason: " + e.getCause().toString());
            if (this.tx != null) {
                this.tx.rollback();
            }
        }
    }
    
    /**
     * <h1>Session Close</h1>
     * <p>
     * This will <strong>end the session</strong> so it <u>won't be reusable again</u>.
     * </p>
     * <p>
     * To start a <b>new session</b>, you must instantiate a new object;
     * Example:
     * </p>
     * <p>
     * hib.closeSession();<br>
     * // Do here something...<br>
     * <strong>hib = new HibernationChamber();</strong>
     * </p>
     * <p>
     * This will make a new session.
     * </p>
     */
    public void closeSession(){
        this.session.close();
    }

}
